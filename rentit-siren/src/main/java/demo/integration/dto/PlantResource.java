package demo.integration.dto;

import lombok.Getter;
import lombok.Setter;

import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

@Getter
@Setter
@Siren4JEntity(name="plant", uri = "http://localhost:8082/rest/plants/{id}")
public class PlantResource extends BaseResource {
	Long id;	
	String name;
	String description;
	Float price;
}
