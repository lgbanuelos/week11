This repository contains three eclipse projects, which we could use for our Lecture on Enterprise Integration Patterns. Basically, we have to major scenarios:

1. Content based routing

> This scenario is associated with the invoicing system described in our project: __RentIt__ can post invoices to __BuildIt__ via email. To showcase XML processing tools, it is assumed that the invoice is sent as an attachment, in the form of an XML file. The project `intgr2` implements a slightly different scenario: BuildIt exposes a Rest end-point that receives the invoice using an HTTP POST. Currently, the http-inbound-gateway component supports only `byte[]` payload. That is why we use a ObjectToString converter. Assuming that the document is an XML file, the next component corresponds to a content-based router. The selector for this router uses a XPATH expression. Please also note how the identifier of the PHR, that is extracted from the URL, is attached to the message header. This is why, the first parameter on `InvoiceManager.process(Long, String)` is annotated as `@Header`.

2. Scatter and Gather

> This scenario is associated with the catalog querying part described in our project: 

Run rentit-siren first (it will serve a plant catalog, but the resources are exposed in SIREN and not in HAL format). Secondly, you should run api-mock with the specification included in `integr2/apiary/rentit.md`. Please note that rentit-siren is configured to run on port 8082. You should run api-mock with port set to 8084. Then, you should query the catalog with a GET on http://localhost:8080/rest/plants

