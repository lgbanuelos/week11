package demo.integration;

import org.springframework.integration.annotation.Header;
import org.springframework.stereotype.Service;

@Service
public class InvoiceManager {
	public void processInvoice(@Header Long phrId, String invoice) {
		System.out.println("Inside process Invoice");
		System.out.println("PHR id: " + phrId);
		System.out.println("Invoice (XML document)\n" + invoice);
	}
}
