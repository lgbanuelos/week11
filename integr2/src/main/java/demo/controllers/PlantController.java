package demo.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import demo.integration.RequestGateway;
import demo.integration.dto.PlantResource;

@Controller
@RequestMapping("/plants")
public class PlantController {
	@Autowired
	RequestGateway rentit;

	@RequestMapping
	public @ResponseBody List<PlantResource> getAllPlants() throws IOException {				
		return rentit.getAllPlants();
	}
	@RequestMapping("/{id}")
	public @ResponseBody PlantResource getPlant(@PathVariable Long id) throws IOException {				
		return rentit.getPlant(id);
	}

}
