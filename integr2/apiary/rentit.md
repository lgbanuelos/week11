FORMAT: 1A
HOST: http://localhost

# ESI14-RentIt
Excerpt of RentIt's API

# Group Purchase Orders
Notes related resources of the **Plants API**

## Plant Catalog [/rest/plants{?name,startDate,endDate}]
### Retrieve Plants [GET]
+ Parameters
    + name (optional,string) ... Name of the plant
    + startDate (optional,date) ... Starting date for rental
    + endDate (optional,date) ... End date for rental

+ Response 200 (application/json)

        [
            {"links":[{"rel":"self", "href":"http://localhost:9000/rest/plants/10001"}], "name":"Excavator", "description":"1.5 Tonne Mini excavator", "price":150.00},
            {"links":[{"rel":"self", "href":"http://localhost:9000/rest/plants/10002"}], "name":"Excavator", "description":"3 Tonne Mini excavator", "price":200.00},
            {"links":[{"rel":"self", "href":"http://localhost:9000/rest/plants/10003"}], "name":"Excavator", "description":"5 Tonne Midi excavator", "price":250.00},
            {"links":[{"rel":"self", "href":"http://localhost:9000/rest/plants/10004"}], "name":"Excavator", "description":"8 Tonne Midi excavator", "price":300.00},
            {"links":[{"rel":"self", "href":"http://localhost:9000/rest/plants/10005"}], "name":"Excavator", "description":"15 Tonne Large excavator", "price":400.00},
            {"links":[{"rel":"self", "href":"http://localhost:9000/rest/plants/10006"}], "name":"Excavator", "description":"20 Tonne Large excavator", "price":450.00}
        ]

## Purchase Order Management [/rest/pos]
### Create Purchase Order [POST]
+ Request (application/json)

        {
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
        }

+ Response 201 (application/json)


        {
            "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/pos/10001" }
                ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
            "cost":500.00
        }

# Group Test Cases
# GET /rest/plants/10001
Available plant
+ Response 200 (application/json)

        {"links":[{"rel":"self", "href":"http://localhost:9000/rest/plants/10001"}], "name":"Excavator", "description":"1.5 Tonne Mini excavator", "price":150.00}

# GET /rest/plants/10002
Plant becomes unavailable by the time a Purchase Order is created
+ Response 409 (application/json)

        {"links":[{"rel":"self", "href":"http://localhost:9000/rest/plants/10002"}], "name":"Excavator", "description":"3 Tonne Mini excavator", "price":200.00}
